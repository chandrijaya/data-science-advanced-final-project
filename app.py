#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Chandra Aldiwijaya
chandra.aldiwijaya.694@gmail.com

Created on Thu Sep 17 09:36:17 2020

@author: bokab
"""

from os import chdir
from os.path import dirname, abspath


def start():
    import program.userInterface as ui
    import program.feature as ftr
    
    fileName = "data_samples.db"
    run = ftr.execute(fileName)
    ctrl = ui.control(run)
    if not run.dataCreated:
        ctrl.cls()
        ctrl.preMenu()
    
    use = True
    count = 0
    logReport = ctrl.status(run.dataCreated)
    ctrl.cls()
    while use:
        ctrl.menu(logReport)
        count += 1
        try:
            decision = int(input("Iter number %i. Choose the option number: " %(count)))
        
        except:
            logReport = "You have to type a number!"
            ctrl.cls()
            continue
        
        ctrl.cls()
        logReport, use = ctrl.mainMenuDecision(decision, count)


if __name__ == "__main__":
    chdir(str(dirname(abspath(__file__))))
    start() 