#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Chandra Aldiwijaya
chandra.aldiwijaya.694@gmail.com

Created on Thu Sep 17 20:58:23 2020

@author: bokab
"""

from datetime import datetime, timedelta
from os import remove, path, mkdir, getcwd
import program.loading as ld
import json


class execute:
    filePath = ""
    
    tokenLoad = False; tokenStat = "No token"
    consumerKey = "xxx"; consumerSecret = "xxx"
    accessToken = "xxx"; accessTokenSecret = "xxx"
    
    searchWord = "vaksin covid"; rangeDays = 2
    connection = 0; cursor = 0
    dataCreated = False; dataUpdated = True; sentimenUpdated = False
    
    dateSince = 0; strSince = ""
    dateUntil = 0; strUntil = ""
    countData = 0; logReport = ""
    
    mean = None; median = None; std = None; summary = None;
    
    def __init__(self, fileName="data_samples.db"):
        import sqlite3
        
        
        if path.isfile("token/token.txt"):
            self.loadToken()
            self.tokenLoad = True
            self.tokenStat = "Testing"
            
        else:
            if not path.isdir(("%s/token" %(getcwd()))):
                mkdir(("%s/token" %(getcwd())))
            
            self.backupToken()
        
        if not path.isdir(("%s/database" %(getcwd()))):
            mkdir(("%s/database" %(getcwd())))
            
        self.filePath = ("database/%s" %(fileName))
        
        self.connection = sqlite3.connect(self.filePath)
        self.cursor = self.connection.cursor()
        try:
            self.cursor.execute('SELECT COUNT(*) from tweet_samples')
            N = (self.cursor.fetchone())[0]
            if N != 0:
                self.countData = N
                self.dataCreated = True
        
        except:
            self.dataCreated = False
            
        if path.isfile("logValue.json") and self.dataCreated:
            self.__loadValue()
        
        self.inputDate()
    
    def __loadValue(self):
        with open("logValue.json") as jsonFile:
            __dict = (json.load(jsonFile))[0]
        
        self.searchWord = __dict["searchWord"]
        self.rangeDays = __dict["rangeDays"]
        self.dataCreated = __dict["dataCreated"]
        self.dataUpdated = __dict["dataUpdated"]
        self.sentimenUpdated = __dict["sentimenUpdated"]
        self.mean = __dict["mean"]
        self.median = __dict["median"]
        self.std = __dict["std"]
        self.summary = __dict["summary"]
    
    def __backupValue(self):
        __dict = [{
                  "searchWord" : self.searchWord,
                  "rangeDays" : self.rangeDays,
                  "dataCreated" : self.dataCreated,
                  "dataUpdated" : self.dataUpdated,
                  "sentimenUpdated" : self.sentimenUpdated,
                  "mean" : self.mean,
                  "median" : self.median,
                  "std" : self.std,
                  "summary" : self.summary
                  }]
        
        with open("logValue.json", 'w') as jsonFile:
            jsonFile.write(json.dumps(__dict, indent=4))
    
    def loadToken(self, txtInput=True):
        if txtInput:
            if path.isfile("token/token.txt"):
                from numpy import loadtxt
                
                f = loadtxt("token/token.txt", dtype=str)
                self.consumerKey = f[0]
                self.consumerSecret = f[1]
                self.accessToken = f[2]
                self.accessTokenSecret = f[3]
                self.logReport = "New API token has reloaded."
                self.tokenLoad = True
                self.tokenStat = "Testing"
            
            else:
                self.logReport = "Error! No 'token.txt' file in {parent}/token directory."
                self.tokenLoad = False
                self.tokenStat = "No token"
            
        else:
            change = False
            self.logReport = "No change for API token."
            cKey = input("Input the consumer key: ")
            if cKey.casefold() == "abort":
                self.logReport = "Manual input new API token has aborted."
                return
                
            cSecret = input("Input the consumer secret: ")
            if cSecret.casefold() == "abort":
                self.logReport = "Manual input new API token has aborted."
                return
            
            aToken = input("Input the access token: ")
            if aToken.casefold() == "abort":
                self.logReport = "Manual input new API token has aborted."
                return
                
            aSecret = input("Input the access token secret: ")
            if aSecret.casefold() == "abort":
                self.logReport = "Manual input new API token has aborted."
                return
            
            if cKey != "":
                self.consumerKey = cKey
                change = True
                
            if cSecret != "":
                self.consumerSecret = cSecret
                change = True
            
            if aToken != "":
                self.accessToken = aToken
                change = True
                
            if aSecret != "":
                self.accessTokenSecret = aSecret
                change = True
            
            if change:
                self.logReport = "New API token has been updated. File 'token,txt' has been updated"
                self.backupToken()
                self.tokenLoad = True
                self.tokenStat = "Testing"
    
    def backupToken(self):
        f = open("token/token.txt", "w")
        f.write("#  Remember! The input order is:\n")
        f.write("#\t1. Consumer Key\n#\t2. Consumer Secret\n#\t3. Access Token\n#\t4. Access Token Secret\n")
        f.write("#  You must input without '#' character on first letter, and leave no blank newline space.\n")
        f.write("#\n#  Input your API token below here.\n")        
        f.write("%s\n" %(self.consumerKey))
        f.write("%s\n" %(self.consumerSecret))
        f.write("%s\n" %(self.accessToken))
        f.write("%s\n" %(self.accessTokenSecret))
        f.close()

    def inputDate(self):
        self.dateSince = datetime.date((datetime.now() - timedelta(days=self.rangeDays)))
        self.dateUntil = datetime.date(datetime.now())
        
        self.strSince = self.dateSince.strftime('%Y-%m-%d')
        self.strUntil = self.dateUntil.strftime('%Y-%m-%d')
    
    def refreshDB(self):
        destroy_query = '''DROP TABLE IF EXISTS tweet_samples;'''
        self.cursor.execute(destroy_query)
        self.connection.commit()
        
        self.dataCreated = False; 
        self.dataUpdated = True; 
        self.sentimenUpdated = False
        
        self.countData = 0; self.logReport = ""
        self.mean = None; self.median = None 
        self.std = None; self.summary = None
    
    def closeDB(self):
        self.cursor.close()
        self.connection.close()
        if path.isfile("logValue.json"):
            remove("logValue.json")
                
        if self.dataCreated:
            self.__backupValue()
            
        else:
            if path.isfile(self.filePath):
                remove(self.filePath)
            
    
    def genData(self):
        import tweepy
        import re
        
        if self.tokenLoad:
            try:
                auth = tweepy.OAuthHandler(self.consumerKey, self.consumerSecret)
                auth.set_access_token(self.accessToken, self.accessTokenSecret)
                api = tweepy.API(auth, wait_on_rate_limit=True)
            
                newSearch = self.searchWord + " -filter:retweets"
            
                tweets = tweepy.Cursor(api.search,
                                       q=newSearch,
                                       lang="id",
                                       since=self.dateSince,
                                       until=self.dateUntil).items()
                
                data = []
                for count, tweet in enumerate(tweets):
                    text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", 
                                           tweet.text).split())
                    text = text.casefold()
                    dateCreated = datetime.date(tweet.created_at)
                    data.append([tweet.id, "@%s" %(tweet.user.screen_name), dateCreated, text])
                    
                    ld.spin(count, "Pre-processing tweet")
                
                self.tokenStat = "Connected"
                
        
            except:
                self.logReport = "Error! There is something wrong with your API token.\nEither wrong token input or the authorization of your token"
                self.tokenStat = "Error"
                return
        
        else:
            self.logReport = "Error! No token loaded"
            self.tokenStat = "No token"
            
    
        n = len(data)
        print("\nGet %i tweet data" %(n))
        
        create_tweet_samples_table = '''CREATE TABLE IF NOT EXISTS tweet_samples (
                                     tweet_id INTEGER PRIMARY KEY NOT NULL,
                                     tweet_user TEXT NOT NULL,
                                     tweet_date TIMESTAMP NOT NULL,
                                     tweet_text TEXT NOT NULL,
                                     sentiment INTEGER);'''
        
        crud_query = '''insert or ignore into tweet_samples (tweet_id, tweet_user, tweet_date, tweet_text) 
                        values (?,?,?,?);'''
        
        self.cursor.execute(create_tweet_samples_table)
        
        self.cursor.execute('SELECT COUNT(*) from tweet_samples')
        initN = (self.cursor.fetchone())[0]
        for count, row in enumerate(data):
            self.cursor.execute(crud_query, row)
            ld.bar(count+1, n, "Generate SQL Data")
        
        self.connection.commit()
        
        self.cursor.execute('SELECT COUNT(*) from tweet_samples')
        self.countData = (self.cursor.fetchone())[0]
        
        self.dataCreated = True
        line2 = ("Database has total %i data." %(self.countData))
        line3 = ("API Tweeter has collected tweet with keyword '%s' from %s to %s" %(self.searchWord, self.strSince, self.strUntil))
        if (initN < self.countData) and (initN != 0):
            self.dataUpdated = True
            self.sentimenUpdated = False
            line1 = ("Database has been updated with new %i entry data." %((self.countData-initN)))
            self.logReport = ("%s\n%s\n%s" %(line1, line2, line3))
            
        else:
            if initN == 0:
                self.dataCreated = True
                self.dataUpdated = True
                self.sentimenUpdated = False
                line1 = ("Database has been created.")
                self.logReport = ("%s\n%s\n%s" %(line1, line2, line3))
                return
            
            self.logReport = ("No update data entry to database!")
            
    
    def sentimenAnalyze(self):
        if not self.dataCreated:
            self.logReport = "Sentiment analyze error! You have to choose 'Data Update' first!"
            return
        
        if not self.dataUpdated:
            self.logReport = "Your data sentiment doesn't need to update"
            return
        
        
        read_query = '''SELECT tweet_id, tweet_text
                     FROM tweet_samples
                     WHERE sentiment IS NULL'''
                     
        update_query = ''' UPDATE tweet_samples
                       SET sentiment = ?
                       WHERE tweet_id = ?'''

        self.cursor.execute(read_query)
        get_sql =  self.cursor.fetchall()
        
        posList= open("sentiment/kata_positif.txt","r")
        posWord = posList.readlines()
        negList= open("sentiment/kata_negatif.txt","r")
        negWord = negList.readlines()
        
        n = len(get_sql)
        for count, row in enumerate(get_sql):
            countP = 0; countN = 0
            tweetId, tweetText = row
            for pos in posWord:
                if pos.strip() in tweetText:
                    countP += 1
                    
            for neg in negWord:
                if neg.strip() in tweetText:
                    countN += 1
                        
            sentimenValue = countP - countN
            data = (sentimenValue, tweetId)
            
            self.cursor.execute(update_query, data)
            ld.bar(count+1, n, "Analyze Tweet Sentiment")
        
        self.connection.commit()
        
        self.dataUpdated = False
        self.sentimenUpdated = True
        
        self.logReport = ("Your data sentiment has updated with %i new value entry" %(n))
            
    def readTweet(self, since="2020-01-01", until="2020-01-01", manualInput=False):
        import os
            
        def cls():
            os.system('cls' if os.name=='nt' else 'clear')
            
        if not self.dataCreated:
            cls()
            self.logReport = "Read database error! You have to choose 'Data Update' first!"
            return True
        
        if manualInput:
            try:
                since = input("Input first date search tweet  (YYYY-MM-DD) : ")
                dateSince = datetime.date(datetime.strptime(since, '%Y-%m-%d'))
                until = input("Input last date search tweet   (YYYY-MM-DD) : ")
                dateUntil = datetime.date(datetime.strptime(until, '%Y-%m-%d'))
                cls()
            
            except:
                if (since.casefold() == "abort") or (until.casefold() == "abort"):
                    self.logReport = "Data visualization aborted."
                    cls()
                    return True
                
                else:
                    self.logReport = "Warning! You input wrong format datetime. Type 'abort' for back to main menu"
                    cls()
                    return False
        
        else:
            dateSince = datetime.date(datetime.strptime(since, '%Y-%m-%d'))
            dateUntil = datetime.date(datetime.strptime(until, '%Y-%m-%d'))
        
        crud_query = '''SELECT tweet_user, tweet_date, tweet_text 
                     FROM tweet_samples 
                     WHERE tweet_date >= ? AND tweet_date <= ?'''
        
        rangeTime = (dateSince.isoformat(), dateUntil.isoformat())
        
        self.cursor.execute(crud_query, rangeTime)
        get_sql =  self.cursor.fetchall()
        
        for row in get_sql:
            a, b, c = row
            tweetDict = {}
            tweetDict["Account"] = a
            tweetDict["Date"] = b
            tweetDict["Tweet"] = c
            print(tweetDict)
        
        print("\x0a")
        
        self.logReport = ("Read %i data since %s until %s" %(len(get_sql), since, until))
        return True
            
    def visualization(self):        
        if not self.dataCreated:
            self.logReport = "Data visualize error! You have to choose 'Data Update' first."
            return
        
        if not self.sentimenUpdated:
            self.logReport = "Your data sentiment need to update first!"
            return
        
        import numpy as np
        import matplotlib.pyplot as plt
        
        crud_query ='''SELECT sentiment FROM tweet_samples'''
    
        self.cursor.execute(crud_query)
        get_sql =  self.cursor.fetchall()
        data = [int(i[0]) for i in get_sql]
       
        self.mean = np.mean(data)
        self.median = np.median(data)
        self.std = np.std(data)
        if (self.mean < 0):
            self.summary = ("The tweeter netizen tend to respond NEGATIVELY for '%s' topic" %(self.searchWord))
        
        else:
            self.summary = ("The tweeter netizen tend to respond POSITIVELY for '%s' topic" %(self.searchWord))
        
        print("Mean: %f" %(self.mean))
        print("Median: %f" %(self.median))
        print("Standard Deviation: %f" %(self.std))
        print("\x0a")
        
        labels, counts = np.unique(data, return_counts=True)
        
        plt.figure()
        plt.bar(labels, counts, align='center')
        plt.gca().set_xticks(labels)
            
        plt.show()
        
        self.logReport = ("Mean = %f\tMedian = %f\tStandard Deviation = %f\n%s" %(self.mean, self.median, self.std, self.summary))
            
