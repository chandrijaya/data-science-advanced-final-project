#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Chandra Aldiwijaya
chandra.aldiwijaya.694@gmail.com

Created on Thu Sep 17 20:42:14 2020

@author: bokab
"""

class control:
    from os import system as osSystem, name as osName
    
    run = None
    
    def __init__(self, run):
        self.run = run
    
    def status(self, dataCreated=False):
        line1 = ("Read file on directory '{parent}/%s' ...\n" %(self.run.filePath))
        line2 = ("Program search tweet with '%s' as a keyword" %(self.run.searchWord))
        line3 = ("Set to search tweet created between %s to %s" %(self.run.strSince, self.run.strUntil))
        line4 = ("For database properties and sentiment values:")
        line5 = ("No readable data on database entry")
        if dataCreated:
            line5 = ("Total: %i\tMean: %s\tMedian: %s\tSTD: %s\nSummary: %s" %(self.run.countData, self.run.mean, self.run.median, 
                     self.run.std, self.run.summary))
        
        line6 = ("Load token: %s\tToken status: %s" %(self.run.tokenLoad, self.run.tokenStat))
            
        return ("%s\n%s\n%s\n%s\n%s\n\n%s" %(line1, line2, line3, line4, line5, line6))
    
    def settingsDecision(self, decision):
        log = ""
        if decision == 1:
            print("Warning! If you change this setting, database will drop main table. This can't be undone.")
            print("The old keyword is '%s'. Leave input blank if you wish to keep it." %(self.run.searchWord))
            newInput = input("Input new keyword: ")
            log = ("No change on keyword.")
            if newInput != "":
                log = ("The keyword has changed from '%s' to '%s" %(self.run.searchWord, newInput))
                self.run.searchWord = newInput
                self.run.refreshDB()
        
        elif decision == 2:
            check = True
            while check:
                print("Warning! If you change this setting, database will drop main table. This can't be undone.")
                print("The old range day(s) is %i day(s). Leave input blank if you wish to keep it." %(self.run.rangeDays))
                try:
                    newInput = input("Input new range day(s) (in day): ")
                    newIntInput = int(newInput)

                    log = ("The range day(s) has changed from '%i' day(s) to '%i day(s)" %(self.run.rangeDays, newIntInput))
                    
                    self.run.rangeDays = newIntInput
                    self.run.inputDate()
                    self.run.refreshDB()
                    
                    check = False
            
                except:
                    if newInput == "":
                        log = ("No change on range day(s).")
                        break
                        
                    self.cls()
                    print("You have to type a number!")
        
        elif decision == 3:
            print("This will rewrite 'token.txt' file in {Parent}/token directory")
            print("Leave it blank if you keep the default value. Type 'abort' if you want to abort change.")
            self.run.loadToken(False)
            log = self.run.logReport
        
        elif decision == 4:
            print("Please input your API token on a 'token.txt' file, \nor copy your own 'token.txt' file to {Parent}/token directory.")
            print("Remember! This is the value order on 'token.txt'")
            print("1. Consumer Key\n2. Consumer Secret\n3. Access Token\n4. Access Token Secret")
            confirm = self.y_n("Are you sure want to reload a new token?")
            log = "Reload new API token has aborted."
            if confirm:
                self.run.loadToken()
                log = self.run.logReport
        
        elif decision == 5:
            confirm = self.y_n("Back to main menu?")
            if confirm:
                log = self.status(self.run.dataCreated)
                return log, False
        
        else:
            log = "You choose wrong number!"
        
        return log, True
        
    
    def mainMenuDecision(self, decision, count):
        log = ""
        if decision == 1:
            print("Iter number %i. You choose to update database." %(count))
            self.run.genData()
            log = self.run.logReport
            self.cls()
            
        elif decision == 2:
            print("Iter number %i. You choose to update sentiment value." %(count))
            self.run.sentimenAnalyze()
            log = self.run.logReport
            self.cls()
            
        elif decision == 3:  
            self.run.logReport = ("Iter number %i. You choose to read a database. Type 'abort' for back to main menu"  %(count))
            check = False
            while not check:
                print(self.run.logReport)
                print("Please pick a date beetween [%s] to [%s]." %(self.run.strSince, self.run.strUntil))
                check = self.run.readTweet(manualInput=True)
            
            log = self.run.logReport
            
        elif decision == 4:
            print("Iter number %i. You choose to visualize a database." %(count))
            self.run.visualization()
            log = self.run.logReport
            self.cls()
                
        elif decision == 5:
            log = self.status(self.run.dataCreated)
        
        elif decision == 6:
            _use = True
            log = ""
            while _use:
                self.menu(log, no_ui=1)
                try:
                    _decision = int(input("Choose the option number: "))
                    self.cls()
                
                except:
                    log = "You have to type a number!"
                    self.cls()
                    continue
                
                log, _use = self.settingsDecision(_decision)
                self.cls()
                
        
        elif decision == 7:
            confirm = self.y_n("Are you sure want to exit?")
            if confirm:
                self.run.closeDB()
                print("Thanks! See you later...")
                print("Code by: sibokab182")
                return log, False
                
            else:
                log = ""
                self.cls()
            
        else:
            log = "You choose wrong number!"
            self.cls()
        
        return log, True
    
    def menu(self, report="", no_ui=0):
        menuHeader = ["[------------------------(Main  Menu)------------------------]",
                      "[-------------------------(Settings)-------------------------]",]
        print("{==================[Sentiment Analyze Apps]==================}\n")
        print("%s\n" %(report))
        print(menuHeader[no_ui])
        ui = [["Update Database.", "Update Sentiment Value.", "Read Database.", 
               "Visualization.", "Program status", "Settings", "Exit."],
              ["Change Keyword.", "Change Range Day(s).", "Manual Input New Token API.", 
               "Reload Token API With 'token.txt'.", "Back to Main Menu"]]
        
        for i, opt in enumerate(ui[no_ui]):
            print("0%i. %s" %(i+1, opt))
    
    def preMenu(self):
        print("{==================[Sentiment Analyze Apps]==================}\n")
        print("No database has found. The program will create it automatically")
        print("This program will search tweet through API tweeter \nusing keyword '%s' from %i day(s) ago" %(self.run.searchWord, self.run.rangeDays))
        confirm = self.y_n("Do you wish to change it?")
        if confirm:
            self.cls()
            self.run.searchWord = input("Input your keyword: ")
            check = True
            while check:
                try:
                    self.run.rangeDays = int(input("input your range day before today: "))
                    self.run.inputDate()
                    check = False
                    
                except:
                    self.cls()
                    print("You have to type a number!")
    
    def y_n(self, info=""):
        use = True
        while use:
            confirm = str(input("%s (Y/n): " %(info))).casefold()
            if confirm == "y":
                use = False
                return True
                
            elif confirm == "n":
                use = False
                return False
            
            else:
                self.cls()
                continue
            
    def cls(self):
        self.osSystem('cls' if self.osName=='nt' else 'clear')
        